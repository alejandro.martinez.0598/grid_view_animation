package com.jalil.ejem08imagenesgrid;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.TranslateAnimation;
import android.view.animation.ScaleAnimation;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.Toast;


import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private GridView gridView = null;
    private ItemAdapter adapter = null;
    private ArrayList<Item> images = null;
    AnimationSet entranceAnimationSet,exitAnimationSet;
    ScaleAnimation entScaleAnim,exitScaleAnim;
    TranslateAnimation entTransAnim,exitTransAnim;
    View selected = null;
    View mainV;
    int midleH , midleW;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        mainV = findViewById(R.id.main);
        gridView = findViewById(R.id.gridView);
        adapter = new ItemAdapter(this,R.layout.row_grid,fillImages());
        midleH = mainV.getHeight()/2;
        midleW = mainV.getWidth()/2;



        entScaleAnim = new ScaleAnimation(1,2,1,2);
        exitScaleAnim = new ScaleAnimation(2,1,2,1);



        gridView.setAdapter(adapter);



        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                //Toast.makeText(getApplicationContext(),""+view.getX(),Toast.LENGTH_SHORT).show();
                view.setBackgroundColor(Color.LTGRAY);

                if( selected != view) {
                    if(selected!=null){
                        makeZoomAfterOut(selected,view);
                    }
                    else
                    makeZoom(view);

                    selected = view;
                }
                else if(selected!=null  ) {
                    makeZoomOut(selected);
                    selected = null;
                }


            }
        });

    }
    private void makeZoomAfterOut(final View in, final View out){

        exitAnimationSet = new AnimationSet(false);
        exitAnimationSet.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                in.setElevation(0);
                makeZoom(out);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        exitTransAnim = new TranslateAnimation(
                Animation.ABSOLUTE, midleW+(selected.getWidth()/2)-selected.getX() ,Animation.RELATIVE_TO_PARENT, 0,
                Animation.ABSOLUTE, midleH+(selected.getHeight()/2)-selected.getY(),Animation.RELATIVE_TO_PARENT, 0);

        exitAnimationSet.addAnimation(exitScaleAnim);
        exitAnimationSet.addAnimation(exitTransAnim);
        exitAnimationSet.setFillAfter(false);
        exitAnimationSet.setDuration(500);


        in.setAnimation(exitAnimationSet);
        exitAnimationSet.start();

    }
    private void makeZoom(View view){
        entranceAnimationSet = new AnimationSet(false);

        entTransAnim = new TranslateAnimation(
                Animation.RELATIVE_TO_PARENT, 0,Animation.ABSOLUTE, midleW+(midleW-view.getX())+(view.getWidth()/2) ,
                Animation.RELATIVE_TO_PARENT, 0,Animation.ABSOLUTE, midleH+(midleH-view.getY())+(view.getHeight()/2));

        entranceAnimationSet.addAnimation(entScaleAnim);
        entranceAnimationSet.addAnimation(entTransAnim);
        entranceAnimationSet.setFillAfter(true);
        entranceAnimationSet.setDuration(500);

        view.setAnimation(entranceAnimationSet);
        entranceAnimationSet.start();
        view.setElevation(5);
    }
    public void makeZoomOut(final View view){
        exitAnimationSet = new AnimationSet(false);
        exitAnimationSet.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                view.setElevation(0);

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        exitTransAnim = new TranslateAnimation(
                Animation.ABSOLUTE, midleW+(selected.getWidth()/2)-selected.getX() ,Animation.RELATIVE_TO_PARENT, 0,
                Animation.ABSOLUTE, midleH+(selected.getHeight()/2)-selected.getY(),Animation.RELATIVE_TO_PARENT, 0);

        exitAnimationSet.addAnimation(exitScaleAnim);
        exitAnimationSet.addAnimation(exitTransAnim);
        exitAnimationSet.setFillAfter(false);
        exitAnimationSet.setDuration(500);


        view.setAnimation(exitAnimationSet);
        exitAnimationSet.start();

    }

    private ArrayList<Item> fillImages() {
        int values = 7;//Integer.parseInt(getResources().getString(R.string.howMany));
        ArrayList<Item> lista = new ArrayList<>();

        for (int i = 0; i <= values; i++) {
            Bitmap bitmap = BitmapFactory.decodeResource(this.getResources(),
                    getResources().getIdentifier("sample_"+i,
                            "drawable",
                            "com.jalil.ejem08imagenesgrid"));
            lista.add(new Item (bitmap,"sample_"+i));
        }
        return lista;
    }
}
